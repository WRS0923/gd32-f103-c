#include "led.h"
#include "gd32f10x_gpio.h"

void LED_Init(void)
{
	rcu_apb2_clock_config(RCU_APB2_CKAHB_DIV1);
	rcu_periph_clock_enable(RCU_GPIOF);

	gpio_init(GPIOF, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_9);
	gpio_init(GPIOF, GPIO_MODE_OUT_PP, GPIO_OSPEED_50MHZ, GPIO_PIN_6);
}
























