#include "gd32f10x.h"
#include "systick.h"
#include "led.h"


int main(void)
{
	systick_config();//系统定时器初始化
	LED_Init();

    while(1)
    {
		gpio_bit_reset(GPIOF, GPIO_PIN_6);
		gpio_bit_reset(GPIOF, GPIO_PIN_9);
		delay_ms(500);
		gpio_bit_set(GPIOF, GPIO_PIN_6);
		gpio_bit_set(GPIOF, GPIO_PIN_9);
		delay_ms(500);
	
    }
	
}




